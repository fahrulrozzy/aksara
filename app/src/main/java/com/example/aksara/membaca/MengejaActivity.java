package com.example.aksara.membaca;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.aksara.R;

public class MengejaActivity extends AppCompatActivity {

    ImageView TampilGambar;
    ImageButton show,hide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mengeja);

        TampilGambar = findViewById(R.id.tampil_mengeja_a);
        show = findViewById(R.id.hurup_b_a);
        hide = findViewById(R.id.hurup_b_b);

        //fungsi untuk animasi
        final Animation animScale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);

        //fungsi show animasi
        show.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                TampilGambar.setVisibility(View.VISIBLE);
            }
        });

        //fungsi hide animasi
        hide.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                TampilGambar.setVisibility(View.INVISIBLE);
            }
        });

        //untuk menambahkan suara
//          final MediaPlayer SuaraAA = MediaPlayer.create(this, R.raw.apple);  // menambahakan suara Apel
//          final MediaPlayer SuaraAB = MediaPlayer.create(this, R.raw.banana);  // menambahkan suara Banana


        // untuk menghubungkan suara ke button
        ImageButton ButtonSuara =  this.findViewById(R.id.hurup_b_a);  //menghubungkan suara Apel ke Button A
        ImageButton ButtonSuara2 =  this.findViewById(R.id.hurup_b_b);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara3 =  this.findViewById(R.id.hurup_b_c);
        ImageButton ButtonSuara4 =  this.findViewById(R.id.hurup_b_d);
        ImageButton ButtonSuara5 =  this.findViewById(R.id.hurup_b_e);
        ImageButton ButtonSuara6 =  this.findViewById(R.id.hurup_b_f);
        ImageButton ButtonSuara7 =  this.findViewById(R.id.hurup_b_g);
        ImageButton ButtonSuara8 =  this.findViewById(R.id.hurup_b_h);
        ImageButton ButtonSuara9 =  this.findViewById(R.id.hurup_b_i);
        ImageButton ButtonSuara10 =  this.findViewById(R.id.hurup_b_j);
        ImageButton ButtonSuara11 =  this.findViewById(R.id.hurup_b_k);
        ImageButton ButtonSuara12 =  this.findViewById(R.id.hurup_b_l);
        ImageButton ButtonSuara13 =  this.findViewById(R.id.hurup_b_m);
        ImageButton ButtonSuara14 =  this.findViewById(R.id.hurup_b_n);
        ImageButton ButtonSuara15 =  this.findViewById(R.id.hurup_b_o);
        ImageButton ButtonSuara16 =  this.findViewById(R.id.hurup_b_p);
        ImageButton ButtonSuara17 =  this.findViewById(R.id.hurup_b_q);
        ImageButton ButtonSuara18 =  this.findViewById(R.id.hurup_b_r);
        ImageButton ButtonSuara19 =  this.findViewById(R.id.hurup_b_s);
        ImageButton ButtonSuara20 =  this.findViewById(R.id.hurup_b_t);
        ImageButton ButtonSuara21 =  this.findViewById(R.id.hurup_b_u);
        ImageButton ButtonSuara22 =  this.findViewById(R.id.hurup_b_v);
        ImageButton ButtonSuara23 =  this.findViewById(R.id.hurup_b_w);
        ImageButton ButtonSuara24 =  this.findViewById(R.id.hurup_b_x);
        ImageButton ButtonSuara25 =  this.findViewById(R.id.hurup_b_y);
        ImageButton ButtonSuara26 =  this.findViewById(R.id.hurup_b_z);




        // untuk menampilkan animasi + suara ketika di klik button
        ButtonSuara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.anjing);
                TampilGambar.startAnimation(animScale);
//                SuaraAA.start();
            }
        });


        ButtonSuara2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.bunga);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.cangkir);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.dunia);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.esia);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.ferrari);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.globe);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.hujan);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.ikan);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.jeruk);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.kapal);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara12.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.lonceng);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara13.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.musik);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara14.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.narator);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara15.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.olahraga);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara16.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.payung);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara17.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.qur_an);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara18.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.roket);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara19.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.surat);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara20.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.tikus);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara21.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.ukulele);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara22.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.voli);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara23.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.wisuda);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara24.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.xenia);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara25.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.yamaha);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara26.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.zebra);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

    }
}
