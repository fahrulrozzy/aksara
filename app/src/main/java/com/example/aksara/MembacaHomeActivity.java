package com.example.aksara;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.aksara.R;
import com.example.aksara.membaca.MembacaHurufActivity;
import com.example.aksara.membaca.MengejaActivity;
import com.example.aksara.membaca.MengenalAngkaActivity;

public class MembacaHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membaca_home);

        Button btn_mengenal_huruf = (Button)findViewById(R.id.btn_mengenal_huruf);
        btn_mengenal_huruf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MembacaHomeActivity.this, MembacaHurufActivity.class);
                startActivity(i);
            }
        });

        Button btn_mengeja = (Button)findViewById(R.id.btn_mengeja);
        btn_mengeja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MembacaHomeActivity.this, MengejaActivity.class);
                startActivity(i);
            }
        });

        Button btn_mengenal_angka = (Button)findViewById(R.id.btn_mengenal_angka);
        btn_mengenal_angka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MembacaHomeActivity.this, MengenalAngkaActivity.class);
                startActivity(i);
            }
        });


    }
}
