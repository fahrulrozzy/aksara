package com.example.aksara;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.aksara.menulis.MenulisAngkaActivity;
import com.example.aksara.menulis.MenulisHurufActivity;

public class MenulisHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menulis_home);

        Button btn_menulis_huruf = (Button)findViewById(R.id.btn_menulis_huruf);
        btn_menulis_huruf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenulisHomeActivity.this, MenulisHurufActivity.class);
                startActivity(i);
            }
        });

        Button btn_menulis_angka = (Button)findViewById(R.id.btn_menulis_angka);
        btn_menulis_angka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenulisHomeActivity.this, MenulisAngkaActivity.class);
                startActivity(i);
            }
        });

    }
}
