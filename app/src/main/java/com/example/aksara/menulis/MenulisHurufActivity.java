package com.example.aksara.menulis;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.aksara.R;

public class MenulisHurufActivity extends AppCompatActivity {

    ImageView TampilGambar;
    ImageButton show,hide;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menulis_huruf);

        TampilGambar = findViewById(R.id.tampil_menulis_a);
        show = findViewById(R.id.hurup_b_a);
        hide = findViewById(R.id.hurup_b_b);

        //fungsi untuk animasi
        final Animation animScale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);

        //fungsi show animasi
        show.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                TampilGambar.setVisibility(View.VISIBLE);
            }
        });

        //fungsi hide animasi
        hide.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                TampilGambar.setVisibility(View.INVISIBLE);
            }
        });


        //untuk menambahkan suara
//          final MediaPlayer SuaraAA = MediaPlayer.create(this, R.raw.apple);  // menambahakan suara Apel
//          final MediaPlayer SuaraAB = MediaPlayer.create(this, R.raw.banana);  // menambahkan suara Banana


        // untuk menghubungkan suara ke button
        ImageButton ButtonSuara =  this.findViewById(R.id.hurup_b_a);  //menghubungkan suara Apel ke Button A
        ImageButton ButtonSuara2 =  this.findViewById(R.id.hurup_b_b);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara3 =  this.findViewById(R.id.hurup_b_c);  //menghubungkan suara Banana ke Button C
        ImageButton ButtonSuara4 =  this.findViewById(R.id.hurup_b_d);  //menghubungkan suara Banana ke Button D
        ImageButton ButtonSuara5 =  this.findViewById(R.id.hurup_b_e);  //menghubungkan suara Banana ke Button E
        ImageButton ButtonSuara6 =  this.findViewById(R.id.hurup_b_f);  //menghubungkan suara Banana ke Button F
        ImageButton ButtonSuara7 =  this.findViewById(R.id.hurup_b_g);  //menghubungkan suara Banana ke Button G
        ImageButton ButtonSuara8 =  this.findViewById(R.id.hurup_b_h);  //menghubungkan suara Banana ke Button H
        ImageButton ButtonSuara9 =  this.findViewById(R.id.hurup_b_i);  //menghubungkan suara Banana ke Button I
        ImageButton ButtonSuara10 =  this.findViewById(R.id.hurup_b_j);  //menghubungkan suara Banana ke Button J
        ImageButton ButtonSuara11 =  this.findViewById(R.id.hurup_b_k);  //menghubungkan suara Banana ke Button K
        ImageButton ButtonSuara12 =  this.findViewById(R.id.hurup_b_l);  //menghubungkan suara Banana ke Button L
        ImageButton ButtonSuara13 =  this.findViewById(R.id.hurup_b_m);  //menghubungkan suara Banana ke Button M
        ImageButton ButtonSuara14 =  this.findViewById(R.id.hurup_b_n);  //menghubungkan suara Banana ke Button N
        ImageButton ButtonSuara15 =  this.findViewById(R.id.hurup_b_o);  //menghubungkan suara Banana ke Button O
        ImageButton ButtonSuara16 =  this.findViewById(R.id.hurup_b_p);  //menghubungkan suara Banana ke Button P
        ImageButton ButtonSuara17 =  this.findViewById(R.id.hurup_b_q);  //menghubungkan suara Banana ke Button Q
        ImageButton ButtonSuara18 =  this.findViewById(R.id.hurup_b_r);  //menghubungkan suara Banana ke Button R
        ImageButton ButtonSuara19 =  this.findViewById(R.id.hurup_b_s);  //menghubungkan suara Banana ke Button S
        ImageButton ButtonSuara20 =  this.findViewById(R.id.hurup_b_t);  //menghubungkan suara Banana ke Button T
        ImageButton ButtonSuara21 =  this.findViewById(R.id.hurup_b_u);  //menghubungkan suara Banana ke Button U
        ImageButton ButtonSuara22 =  this.findViewById(R.id.hurup_b_v);  //menghubungkan suara Banana ke Button V
        ImageButton ButtonSuara23 =  this.findViewById(R.id.hurup_b_w);  //menghubungkan suara Banana ke Button W
        ImageButton ButtonSuara24 =  this.findViewById(R.id.hurup_b_x);  //menghubungkan suara Banana ke Button X
        ImageButton ButtonSuara25 =  this.findViewById(R.id.hurup_b_y);  //menghubungkan suara Banana ke Button Y
        ImageButton ButtonSuara26 =  this.findViewById(R.id.hurup_b_z);  //menghubungkan suara Banana ke Button Z




        // untuk menampilkan animasi + suara ketika di klik button
        ButtonSuara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_a);
                TampilGambar.startAnimation(animScale);
//                SuaraAA.start();
            }
        });


        ButtonSuara2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_b);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_c);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_d);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_e);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_f);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_g);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_h);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_i);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_j);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_k);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara12.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_l);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara13.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_m);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara14.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_n);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara15.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_o);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara16.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_p);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara17.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_q);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara18.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_r);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara19.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_s);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara20.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_t);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara21.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_u);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara22.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_v);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara23.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_w);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara24.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_x);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara25.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_y);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });
        ButtonSuara26.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_z);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

    }
}
