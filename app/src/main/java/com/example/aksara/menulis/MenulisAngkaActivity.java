package com.example.aksara.menulis;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.aksara.R;

public class MenulisAngkaActivity extends AppCompatActivity {

    ImageView TampilGambar;
    ImageButton show,hide;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menulis_angka);

        TampilGambar = findViewById(R.id.tampil_menulis_1);
        show = findViewById(R.id.hurup_b_1);
        hide = findViewById(R.id.hurup_b_2);

        //fungsi untuk animasi
        final Animation animScale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);

        //fungsi show animasi
        show.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                TampilGambar.setVisibility(View.VISIBLE);
            }
        });

        //fungsi hide animasi
        hide.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                TampilGambar.setVisibility(View.INVISIBLE);
            }
        });


        //untuk menambahkan suara
//          final MediaPlayer SuaraAA = MediaPlayer.create(this, R.raw.apple);  // menambahakan suara Apel
//          final MediaPlayer SuaraAB = MediaPlayer.create(this, R.raw.banana);  // menambahkan suara Banana


        // untuk menghubungkan suara ke button
        ImageButton ButtonSuara =  this.findViewById(R.id.hurup_b_1);  //menghubungkan suara Apel ke Button A
        ImageButton ButtonSuara2 =  this.findViewById(R.id.hurup_b_2);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara3 =  this.findViewById(R.id.hurup_b_3);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara4 =  this.findViewById(R.id.hurup_b_4);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara5 =  this.findViewById(R.id.hurup_b_5);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara6 =  this.findViewById(R.id.hurup_b_6);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara7 =  this.findViewById(R.id.hurup_b_7);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara8 =  this.findViewById(R.id.hurup_b_8);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara9 =  this.findViewById(R.id.hurup_b_9);  //menghubungkan suara Banana ke Button B
        ImageButton ButtonSuara0 =  this.findViewById(R.id.hurup_b_0);  //menghubungkan suara Banana ke Button B




        // untuk menampilkan animasi + suara ketika di klik button
        ButtonSuara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_satu);
                TampilGambar.startAnimation(animScale);
//                SuaraAA.start();
            }
        });


        ButtonSuara2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_dua);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_tiga);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_empat);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_lima);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_enam);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_tujuh);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_delapan);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_sembilan);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });

        ButtonSuara0.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.menulis_nol);
                TampilGambar.startAnimation(animScale);
//                SuaraAB.start();
            }
        });


    }
}
